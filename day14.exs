defmodule Day14 do
  @moduledoc """
  Used to solve Day 14.  To get a list of scores under part 1 for time `t` run Day14.travel(t).  To select
  only the Reindeer with the largest score run the second example.

      iex> Day14.travel(2053)

      [{"Blitzen", 2142}, {"Comet", 2052}, {"Cupid", 2112}, {"Dancer", 2025},
      {"Dasher", 1856}, {"Donner", 2100}, {"Prancer", 2142}, {"Rudolph", 2145},
      {"Vixen", 2160}]

      iex> Day14.travel(2053) |> Enum.max_by(fn {_x, y} -> y end)

      {"Vixen", 2160}
  """
  @input "/Users/spencermurray/workspace/advent/inputs/day14.txt"

  def formatinput do
    @input
    |> File.read!
    |> String.strip
    |> String.split("\n", trim: true)
    |> Enum.map(&String.split/1)
  end

  #pass get_speeds to Enum.reduce.  Run how_far in Enum.reduce and acc distances to list acc.
  def travel(time) do
    get_speeds
    |> Dict.keys
    |> Enum.reduce([], fn(reindeer, acc) -> acc ++ [{reindeer, how_far(reindeer, time)}] end)
  end

  def get_speeds do
    formatinput
    |> Enum.reduce(%{}, fn (x, acc) ->
      [reindeer, _, _, speed, _, _, distance, _, _, _, _, _, _, rest, _] = x
      Dict.put(acc, reindeer, {speed, distance, rest})
    end)
  end

  def how_far(reindeer, time) do
    {speed, distance, rest} = Dict.get(get_speeds, reindeer)
    how_far(String.to_integer(speed), String.to_integer(distance), String.to_integer(rest), time)
  end

  defp how_far(speed, distance, rest, time, covered \\ 0, counter \\ 0, total \\ 0) do
    case total do
      ^time -> covered
      _ ->  case counter do
              ^distance -> rester(speed, distance, rest, time, covered, 0, total + 1)
              _ -> how_far(speed, distance, rest, time, covered + speed, counter + 1, total + 1)
            end
    end
  end

  def rester(speed, distance, rest, time, covered, counter, total) do
    case total do
      ^time -> covered
      _ ->
        cond do
          counter == rest - 1 -> how_far(speed, distance, rest, time, covered + speed, 1, total + 1)
          counter < rest -> rester(speed, distance, rest, time, covered, counter + 1, total + 1)
        end
     end
  end
end

defmodule Day14.Part2 do
  @moduledoc """
  Used to answer Part 2 of Day 14.  To return a list of all of the scores at time "t" run Day14.Part2.runner(t).
  To select only the largest value run the above piped into Enum.max_by(fn{_x, y} -> y end)
  ## Examples

      iex> Day14.Part2.runner 2503

      %{"Blitzen" => 6, "Comet" => 213, "Cupid" => 46, "Dancer" => 164,
      "Donner" => 1102, "Prancer" => 176, "Rudolph" => 647, "Vixen" => 360}

      iex> Day14.Part2.runner(2503) |> Enum.max_by(fn {_x, y} -> y end)

      {"Donner", 1102}
  """
  def runner(time) do
    1..time
    |> Enum.reduce(%{}, fn(x, acc) ->
      Day14.travel(x) |> combine(acc)
    end)
  end

  def max_distance(collection) do
    {_reindeer, distance} = collection
    |> Enum.max_by(fn{_x, y} -> y end)
    distance
  end

  def combine(collection, dict) do
    collection
    |> Enum.reduce(dict, fn({reindeer, distance}, acc) -> 
      cond do
        distance == max_distance(collection) -> 
          current_score = Dict.get(acc, reindeer)
          case current_score do
            nil -> Dict.put(acc, reindeer, 1)
            _ -> Dict.put(acc, reindeer, current_score + 1)
          end
        true -> acc
      end
      end)
  end
end