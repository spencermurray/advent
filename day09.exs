defmodule Day9 do
    @input "/Users/spencermurray/workspace/advent/inputs/day9.txt"

    def formatinput do
      @input
      |> File.read!
      |> String.split("\n", trim: true)
      |> Enum.map(&String.split/1)
    end

    def city_travel do
      city_list
      |> permutations
      |> Enum.reduce([], fn(permutation, acc) -> acc ++ [permutation_length(permutation)] end)
    end

    #pipe city_list into this to generate all possible permutations
    def permutations([]) do [[]] end
    def permutations(list) do
      for h <- list, t <- permutations(list -- [h]), do: [h | t]
    end

    def city_list do
      formatinput
      |> Enum.reduce([], fn(x, acc) ->
        [start, _, finish, _, _distance] = x
        acc ++ [start] ++ [finish] end)
      |> Enum.uniq
      |> Enum.sort
    end

    def get_distance(begin, finish) do
      begin
      |> distance_find
      |> Enum.filter(fn[city, _distance] -> city == finish end)
      |> List.flatten
      |> List.last
      |> String.to_integer
    end

    def permutation_length(_permutation, last \\ "", total \\ 0)
    def permutation_length(_permutation = [], _, total), do: total
    def permutation_length(_permutation = [h | t], last, total) do
      case last do
        "" -> permutation_length(t, h, total)
        _ ->  distance = get_distance(last, h)
              permutation_length(t, h, total + distance)
      end
    end

    def distance_find(city) do
      formatinput
      |> Enum.reduce([], fn(x, acc) ->
        [start, _, finish, _, distance] = x
        cond do
          city == start -> acc ++ [[finish, distance]]
          city == finish -> acc ++ [[start, distance]]
          true -> acc
        end
      end)
    end
    def travel_candidates(city) do
      formatinput
      |> Enum.reduce([], fn(x, acc) ->
        [start, _, finish, _, _distance] = x
        # case start do
        cond do 
          city == start -> acc ++ [finish]
          city == finish -> acc ++ [start]
          true-> acc
        end
      end)
    end
end

IO.puts "Part 1: The shortest distance is #{Day9.city_travel |> Enum.min}"
IO.puts "Part 2: The longest distance is #{Day9.city_travel |> Enum.max}"