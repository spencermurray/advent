defmodule Day6 do
  @input "/Users/spencermurray/workspace/advent/inputs/day6.txt"

  def input do
    @input
    |> File.read!
    |> String.strip
    |> String.split("\n")
    |> Enum.map(&String.split/1)
    |> Enum.map(&format_input/1)
  end

  #Take a line from from the input and format it into a {:type, begin, end} command using pattern matching

  def count_the_lights do
    Enum.reduce(input, init_lights(:off), fn(instruction, lights) ->
      case instruction do
        {:on, begin, ending} -> on(generate_coordinates(begin, ending), lights)
        {:off, begin, ending} -> off(generate_coordinates(begin, ending), lights)
        {:toggle, begin, ending} -> toggle(generate_coordinates(begin, ending), lights)
      end
    end)
    |> Enum.count(fn({_light, state}) -> state == :on end)
  end

  defp on(range, lights) do
    Enum.reduce(range, lights, fn(value, acc) -> HashDict.put(acc, value, :on) end)
  end
  defp off(range, lights) do
    Enum.reduce(range, lights, fn(value, acc) -> HashDict.put(acc, value, :off) end)
  end
  defp toggle(range, lights) do
    Enum.reduce(range, lights, fn(value, acc) -> state = HashDict.get(acc, value)
    case state do
      :on -> HashDict.put(acc, value, :off)
      :off -> HashDict.put(acc, value, :on)
    end
    end)
  end

  #Enum.reduce(lights, HashDict.new, fn(x, acc) -> HashDict.put(acc, x, :off) end)
  defp generate_coordinates({x, y}, {xx, yy}) do
    for x <- x..xx, y <- y..yy, do: {x, y}
  end
  #Set up a grid of lights where all values are initially off
  def init_lights(state) do
    generate_coordinates({0, 0}, {999, 999})
    |> Enum.reduce(HashDict.new, fn(value, acc) -> HashDict.put(acc, value, state) end)
  end

  defp format_input([_, "on", begin, _, ending]), do: commands(:on, begin, ending)
  defp format_input([_, "off", begin, _, ending]), do: commands(:off, begin, ending)
  defp format_input(["toggle", begin, _, ending]), do: commands(:toggle, begin, ending)

  defp commands(command, begin, ending) do
    [x, y] = String.split(begin, ",") |> Enum.map(&String.to_integer/1)
    [xx, yy] = String.split(ending, ",") |> Enum.map(&String.to_integer/1)
    {command, {x, y}, {xx, yy}}
  end
end

defmodule Day6.Part2 do
  @input "/Users/spencermurray/workspace/advent/inputs/day6.txt"
  
  def input do
    @input
    |> File.read!
    |> String.strip
    |> String.split("\n")
    |> Enum.map(&String.split/1)
    |> Enum.map(&format_input/1)
  end

  #Take a line from from the input and format it into a {:type, begin, end} command using pattern matching

  def count_the_lights do
    Enum.reduce(input, init_lights(0), fn(instruction, lights) ->
      case instruction do
        {:on, begin, ending} -> on(generate_coordinates(begin, ending), lights)
        {:off, begin, ending} -> off(generate_coordinates(begin, ending), lights)
        {:toggle, begin, ending} -> toggle(generate_coordinates(begin, ending), lights)
      end
    end)
    |> Enum.reduce(0, fn({_light, brightness}, acc) -> brightness + acc end)
  end

  defp on(range, lights) do
    Enum.reduce(range, lights, fn(value, acc) -> 
      HashDict.update!(acc, value, fn(current) -> current + 1 end )
    end)
  end

  defp off(range, lights) do
    Enum.reduce(range, lights, fn(value, acc) -> 
      HashDict.update!(acc, value, fn
        current when current > 0 -> current - 1
        current -> current
    end)
    end)
  end
 defp toggle(range, lights) do
    Enum.reduce(range, lights, fn(value, acc) -> 
      HashDict.update!(acc, value, fn(current) -> current + 2 end )
    end)
  end

  #Enum.reduce(lights, HashDict.new, fn(x, acc) -> HashDict.put(acc, x, :off) end)
  defp generate_coordinates({x, y}, {xx, yy}) do
    for x <- x..xx, y <- y..yy, do: {x, y}
  end
  #Set up a grid of lights where all values are initially off
  def init_lights(brightness) do
    generate_coordinates({0, 0}, {999, 999})
    |> Enum.reduce(HashDict.new, fn(value, acc) -> HashDict.put(acc, value, brightness) end)
  end

  defp format_input([_, "on", begin, _, ending]), do: commands(:on, begin, ending)
  defp format_input([_, "off", begin, _, ending]), do: commands(:off, begin, ending)
  defp format_input(["toggle", begin, _, ending]), do: commands(:toggle, begin, ending)

  defp commands(command, begin, ending) do
    [x, y] = String.split(begin, ",") |> Enum.map(&String.to_integer/1)
    [xx, yy] = String.split(ending, ",") |> Enum.map(&String.to_integer/1)
    {command, {x, y}, {xx, yy}}
  end
end

IO.puts "Part 1: #{Day6.count_the_lights}"
IO.puts "Part 2: #{Day6.Part2.count_the_lights}"
