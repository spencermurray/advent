defmodule Day10 do
  def run(num, times) when is_integer(num) do
     run(to_string(num), times)
  end
  def run(num, times) do
    Enum.reduce(1..times, num, fn(_, string) -> Day10.look_and_say(string) end)
    |> String.length
  end

  def look_and_say(num) do
    num
    |> String.codepoints
    |> formatter(0, "", "")
  end

  def formatter([], count, tracker, newstring) do
    if tracker != 0 do
      newstring <> to_string(count) <> tracker
    else
      newstring
    end
  end
  def formatter(_string = [h | t], count, tracker, newstring) do
    case tracker do
      ^h -> formatter(t, count + 1, h, newstring)
      "" -> formatter(t, 1, h, newstring)
      _ -> formatter(t, 1, h, newstring <> to_string(count) <> tracker)
    end
  end
end

IO.puts "Part 1: #{Day10.run(3113322113, 40)}"
IO.puts "Part 2: #{Day10.run(3113322113, 50)}"