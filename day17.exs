defmodule Day17 do
  @input "/Users/spencermurray/workspace/advent/inputs/day17.txt"
  def formatinput do
    @input
    |> File.read!
    |> String.split("\n", trim: true)
    |> Enum.map(&String.to_integer/1)
  end

  def run(num) do
    summer
    |> Enum.reduce(0, fn(x, acc) ->
      case x do
        ^num -> acc + 1
        _ -> acc
      end
    end)
  end

  def find_min(num) do
    Enum.reduce(1..Enum.count(formatinput), [], fn (x,acc) ->
      cond do
        combinations(formatinput, x) |> Enum.map(&Enum.sum/1) |> Enum.member?(num) -> acc ++ [x]
        true -> acc
      end
    end)
    |> Enum.min
  end

  def part2(num) do
    min = find_min(num)
    formatinput
    |> combinations(min)
    |> Enum.map(&Enum.sum/1)
    |> Enum.count(fn x -> x == num end)
  end

  def summer do
    Enum.reduce(1..Enum.count(formatinput), [], fn(x, acc) ->
          t = combinations(Day17.formatinput, x) |> Enum.map(&Enum.sum/1)
          acc ++ t
    end)
  end

  def combinations(_, 0), do: [[]]
  def combinations([], _), do: []
  def combinations([x|xs], n) do
    (for y <- combinations(xs, n - 1), do: [x|y]) ++ combinations(xs, n)
  end
end

IO.puts "Part 1: #{Day17.run(150)}"
IO.puts "Part 2: #{Day17.part2(150)}"
