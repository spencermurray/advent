defmodule Day13 do
  @input "/Users/spencermurray/workspace/advent/inputs/day13.txt"

  def formatinput do
    @input
    |> File.read!
    |> String.replace(".", "")
    |> String.split("\n", trim: true)
    |> Enum.map(&String.split/1)
  end

  def run do
    scores = happiness_scores
    people_list
    |> permutations
    |> Enum.reduce([], fn(arrangement, acc) -> acc ++ [calc_happiness(arrangement, scores)] end)
    |> Enum.max
  end

  #generate a HashDict of scores for everyone to reduce program run time
  def happiness_scores do
    Enum.reduce(people_list, HashDict.new, fn(person, acc) -> 
      Enum.reduce(people_list -- [person], acc, fn(new, dict) ->
        HashDict.put(dict, {person, new}, happiness(person, new)) end)
    end)
  end

  def calc_happiness(permutation = [h | t], scores) do
    calc_happiness(List.last(t), permutation, 0, h, scores)
  end

  defp calc_happiness(left, [h | []], p, start, scores) do
    p + HashDict.get(scores, {h, left}) + HashDict.get(scores, {h, start})
  end
  defp calc_happiness(left, [person | t], p, start, scores) do
    happy = p + HashDict.get(scores, {person, left}) + HashDict.get(scores, {person, List.first(t)})
    calc_happiness(person, t, happy, start, scores)
  end

  def happiness(person, adjacent) do
    person_happiness(person)
    |> Enum.reduce(0, fn([p, h], acc) ->
      case p do
        ^adjacent -> acc + h
        _ -> acc
      end
    end)
  end

  def person_happiness(person) do
    formatinput
    |> Enum.reduce([], fn(x, acc) ->
      [seat, _, change, amount, _, _, _, _, _, _, adjacent] = x
      if change == "gain" do
        case seat do
          ^person -> acc ++ [[adjacent, String.to_integer(amount)]]
          _ -> acc
        end
      else
        case seat do
          ^person -> acc ++ [[adjacent, String.to_integer(amount) * -1]]
          _ -> acc
        end
      end
    end)
  end

  def people_list do
    formatinput
    |> Enum.reduce([], fn(line, acc) -> acc ++ [line |> List.first] ++ [line |> List.last] end)
    |> Enum.uniq
  end

  def permutations([]), do: [[]]
  def permutations(list) do
    for h <- list, t <- permutations(list -- [h]), do: [h | t]
  end
end

defmodule Day13.Part2 do
  def run do
    scores = happiness_scores
    Day13.people_list ++ ["Me"]
    |> Day13.permutations
    |> Enum.reduce([], fn(arrangement, acc) -> acc ++ [Day13.calc_happiness(arrangement, scores)] end)
    |> Enum.max
  end

  def happiness_scores do
    Enum.reduce(Day13.people_list ++ ["Me"], HashDict.new, fn(person, acc) -> 
      Enum.reduce((Day13.people_list ++ ["Me"]) -- [person], acc, fn(new, dict) ->
        HashDict.put(dict, {person, new}, happiness(person, new)) end)
    end)
  end

  def happiness(person, adjacent) do
    cond do
      person == "Me" || adjacent == "Me" -> 0
      true -> Day13.person_happiness(person)
              |> Enum.reduce(0, fn([p, h], acc) ->
                case p do
                  ^adjacent -> acc + h
                  _ -> acc
                end
              end)
    end
  end
end