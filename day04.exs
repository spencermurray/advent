defmodule Crypto do
  def md5(data), do: Base.encode16(:erlang.md5(data), case: :lower)
end

defmodule Day4 do
  import Crypto
  def adventcode(hash \\ "", count \\ 0, key \\ "ckczppom") do
    case hash do
      "00000" -> count
      _ ->  md5(key <> to_string(count + 1)) |> String.slice(0, 5) |> adventcode(count + 1)
    end    
  end  
end

defmodule Day4.Part2 do
  import Crypto
  def adventcode(hash \\ "", count \\ 0, key \\ "ckczppom") do
    case hash do
      "000000" -> count
      _ ->  md5(key <> to_string(count + 1)) |> String.slice(0, 6) |> adventcode(count + 1)
    end
  end  
end

IO.puts "Part 1: #{Day4.adventcode}"
IO.puts "Part 2: #{Day4.Part2.adventcode}"