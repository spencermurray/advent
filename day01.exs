defmodule Day1 do
  def counter(list, count \\ 0)
  def counter([head | tail], count) do
    case head do
      ?( -> counter(tail, count + 1)
      ?) -> counter(tail, count - 1)
    end
  end

  def counter([], count), do: count
end

defmodule Day1_2 do
  def counter(list, count \\ 0, steps \\ 0)
  def counter(_, count, steps) when count < 0, do: steps
  def counter([head | tail], count, steps) do
    case head do
      ?( -> counter(tail, count + 1, steps + 1)
      ?) -> counter(tail, count - 1, steps + 1)
    end
  end
end